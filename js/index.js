'use strict';
//конструктор навигационного меню
function navigation(options){
	this._elem = options.elem;
}
//методы
navigation.prototype.getXmlHttpRequest = function(){
	if (window.XMLHttpRequest) 
		{
			try 
			{
				return new XMLHttpRequest();
			} 
			catch (e){}
		} 
		else if (window.ActiveXObject) 
		{
			try 
			{
				return new ActiveXObject('Msxml2.XMLHTTP');
			} catch (e){}
			try 
			{
				return new ActiveXObject('Microsoft.XMLHTTP');
			} 
			catch (e){}
		}
		return null;
};
// подгрука контента по ссылке
navigation.prototype.loadLink = function(link, async){
	let xhr = this.getXmlHttpRequest();
	if(async){
		xhr.timeout = 30000;
		xhr.ontimeout = function(){
			alert('The request has expired!');
		}
	}
		xhr.onload = function(){
			$('section.section_hosting').html(xhr.responseText);

			switch(link){
				case './home.html':{
					detailsInit();
					break;
				}
			}
		};
		xhr.onerror = function(){
			console.log("Статус: " + xhr.status + " " + xhr.statusText);
		};
		xhr.open('POST', link, async);
		xhr.send(null);
};
navigation.prototype.getElem = function(){
	return this._elem;
};
navigation.prototype.init = function(){
	let thsObj = this;
	this._elem.addEventListener('click', function(e){
		let target = e.target;
		console.log(thsObj);
		thsObj.loadLink(target.getAttribute('data-href'), true);
	});
};

$(function(){
	var menu = new navigation({
		elem: document.getElementById('navigation')
	});

	menu.init();
	menu.loadLink(menu.getElem().firstElementChild.getAttribute('data-href'), true);
});