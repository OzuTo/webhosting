'use strict';
//конструктор details
function details(options) {
	navigation.apply(this, arguments);
	this._button = options.button;
	this._objToJSON = {};
	this._visibl = 'none';
	this._strJSON;
}
//наследование
details.prototype = Object.create(navigation.prototype);
//сохраняем конструктор LeftMenu
details.prototype.constructor = details;
details.prototype.init = function(){
	let thsObj = this;
	this._button.on('click', function(event){
		let target = (event.target.tagName === 'SPAN')? event.target.parentElement : event.target;
		thsObj._visibl = (thsObj._visibl === 'none')? 'block' : 'none';
		thsObj._elem.css({'display': thsObj._visibl});
		thsObj._objToJSON.size = target.getAttribute('id');
	});
	
	let checkDomain = this._elem.find('div.registerDomain div.checkDomain');
	let checkBox = this._elem.find('div.registerDomain form div.collectionCheckBox ul li input[type="checkbox"]');
	let nameDomain = thsObj._elem.find('div.registerDomain form input[name = "domainName"]');
	
	checkDomain.on('click', function(event){
		thsObj._objToJSON.name = (nameDomain.val() === '')? 'none' : nameDomain.val();
		$.each(checkBox, function(key, val){
			if (val.checked) {
				thsObj._objToJSON[val.getAttribute('name')] = 'true';
			}
		});
		thsObj._strJSON = JSON.stringify(thsObj._objToJSON);
		//отправка запроса на сервер о созданиие нового домена
		//thsObj.loadLink('',true);
		thsObj._visibl = (thsObj._visibl === 'none')? 'block' : 'none';
		thsObj._elem.css({'display': thsObj._visibl});
	});
};
details.prototype.loadLink = function(link, async){
	let xhr = this.getXmlHttpRequest();
	if(async){
		xhr.timeout = 30000;
		xhr.ontimeout = function(){
			alert('The request has expired!');
		}
	}
	xhr.setRequestHeader('domain', 'new');
	xhr.onload = function(){
		alert('OK!');
	};
		xhr.onerror = function(){
			console.log("Статус: " + xhr.status + " " + xhr.statusText);
		};
		xhr.open('POST', link, async);
		xhr.send(this._strJSON);
};

function detailsInit(){
	var dtls = new details({
		elem: $('section.section_hosting div.details'),
		button: $('.section_hosting div.hosting article div.button')
	});
	dtls.init();
}